//
// Created by mints on 26.12.22.
//

#ifndef GRAVITY_APP_MOVEMENT_H
#define GRAVITY_APP_MOVEMENT_H

#include <set>
#include <vector>
#include "objects.h"
#include "collision.h"

class Movement
{
private:
  struct CollisionPair
  {
    CollisionPair(BaseGravityObject* theObj1, BaseGravityObject* theObj2, double theT) :
            mObj1(theObj1), mObj2(theObj2), mT(theT) {};
    bool operator<(const CollisionPair& thePair) const
    {
      if (mObj1 < thePair.mObj1)
      {
        return true;
      }
      else if (mObj1 == thePair.mObj1)
      {
          return mObj2 < thePair.mObj2;
      }
      return false;
    };
    BaseGravityObject* mObj1;
    BaseGravityObject* mObj2;
    double mT;
  };
public:
  Movement(std::vector<BaseGravityObject*> *theObjs,
           double theMinX,
           double theMaxX,
           double theMinY,
           double theMaxY,
           double theTolerance);

  void Move(double theInterval);
private:
  static void MakeRebound(const CollisionPair& thePair);
private:
  double mMinX;
  double mMaxX;
  double mMinY;
  double mMaxY;
  double mTolerance;
  std::vector<BaseGravityObject*> *mObjs;
  std::set<CollisionPair> mFlyingApart;
};

#endif //GRAVITY_APP_MOVEMENT_H
