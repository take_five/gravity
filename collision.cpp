//
// Created by mints on 25.12.22.
//
#include <algorithm>
#include <cmath>
#include <memory>
#include "collision.h"

bool FindCollisionOnInterval(const BaseGravityObject &theObj1,
                             const BaseGravityObject &theObj2,
                             double theInterval,
                             double theTolerance,
                             double& theFoundT)
{
  // get the max velocity of objects on interval without collisions
  CoordXY aMaxVel1 = BaseGravityObject::GetMaxVel(theObj1.GetVel(), theInterval);
  CoordXY aMaxVel2 = BaseGravityObject::GetMaxVel(theObj2.GetVel(), theInterval);
  double aMaxVel = std::max(std::max(fabs(aMaxVel1.x), fabs(aMaxVel1.y)), std::max(fabs(aMaxVel2.x), fabs(aMaxVel2.y)));
  // get the time step
  double aStepT = theTolerance / (aMaxVel * 4.0);

  // make the copies of Object1 and Object2 to move them
  std::unique_ptr<BaseGravityObject> aCopyObj1(theObj1.GetCopy());
  std::unique_ptr<BaseGravityObject> aCopyObj2(theObj2.GetCopy());

  // scan through the interval
  double aCurT = 0.0;
  while (aCurT < theInterval)
  {
    if (DetectCollision(*aCopyObj1, *aCopyObj2))
    {
      theFoundT = aCurT;
      return true;
    }
    aCopyObj1->Move(aStepT);
    aCopyObj2->Move(aStepT);
    aCurT += aStepT;
  }

  return false;
}

bool DetectCollision(const BaseGravityObject &theObj1, const BaseGravityObject &theObj2)
{
  auto aCircle1 = dynamic_cast<const GravityCircle*>(&theObj1);
  auto aCircle2 = dynamic_cast<const GravityCircle*>(&theObj2);
  const GravitySquare* aSquare1 = nullptr;
  const GravitySquare* aSquare2 = nullptr;

  if (aCircle1 == nullptr)
  {
    aSquare1 = dynamic_cast<const GravitySquare*>(&theObj1);
  }
  if (aCircle2 == nullptr)
  {
    aSquare2 = dynamic_cast<const GravitySquare *>(&theObj2);
  }
  if (aCircle1 != nullptr && aCircle2 != nullptr)
  {
    return CircleCircleIntersect(*aCircle1, *aCircle2);
  }
  else if (aCircle1 != nullptr && aSquare2 != nullptr)
  {
    return CircleSquareIntersect(*aCircle1, *aSquare2);
  }
  else if (aSquare1 != nullptr && aCircle2 != nullptr)
  {
    return CircleSquareIntersect(*aCircle2, *aSquare1);
  }
  else if (aSquare1 != nullptr && aSquare2 != nullptr)
  {
    return SquareSquareIntersect(*aSquare1, *aSquare2);
  }

  return false;
}

bool CircleCircleIntersect(const GravityCircle &theObj1, const GravityCircle &theObj2)
{
  double aDeltaX = theObj1.GetPos().x - theObj2.GetPos().x;
  double aDeltaY = theObj1.GetPos().y - theObj2.GetPos().y;
  double aRadii = theObj1.GetRadius() + theObj2.GetRadius();

  return (aDeltaX * aDeltaX + aDeltaY * aDeltaY) < aRadii * aRadii;
}

bool CircleSquareIntersect(const GravityCircle &theObj1, const GravitySquare &theObj2)
{
  // rotate matrix for Square
  double aRotateM[4];
  aRotateM[0] =  cos(theObj2.GetAngle());
  aRotateM[1] = -sin(theObj2.GetAngle());
  aRotateM[2] = -aRotateM[1];
  aRotateM[3] =  aRotateM[0];

  // the coords of each corner of Square1
  CoordXY aCorner[4];
  double aHalfSize = 0.5 * theObj2.GetLength();
  aCorner[0].x = -aHalfSize;
  aCorner[0].y = -aHalfSize;
  aCorner[1].x = -aHalfSize;
  aCorner[1].y =  aHalfSize;
  aCorner[2].x =  aHalfSize;
  aCorner[2].y =  aHalfSize;
  aCorner[3].x =  aHalfSize;
  aCorner[3].y = -aHalfSize;

  double aRadius2 = theObj1.GetRadius() * theObj1.GetRadius();

  for (int i = 0; i < 4; i++)
  {
    double aX, aY, aDeltaX, aDeltaY;
    // rotate the corner to Square angle
    aX = aCorner[i].x * aRotateM[0] + aCorner[i].y * aRotateM[1];
    aY = aCorner[i].x * aRotateM[2] + aCorner[i].y * aRotateM[3];
    // move the corner to Square position and back to Circle position
    aDeltaX = theObj2.GetPos().x + aX - theObj1.GetPos().x;
    aDeltaY = theObj2.GetPos().y + aY - theObj1.GetPos().y;
    // check if the corner of Square is inside Circle
    if (aDeltaX * aDeltaX + aDeltaY * aDeltaY < aRadius2)
    {
      return true;
    }
  }

  // ===============================================================

  double aX1, aY1, aX2, aY2;
  // move the center of Circle to Circle position and back to Square position
  aX1 = theObj1.GetPos().x - theObj2.GetPos().x;
  aY1 = theObj1.GetPos().y - theObj2.GetPos().y;
  // rotate the center to Square -angle
  aX2 =  aX1 * aRotateM[0] - aY1 * aRotateM[1];
  aY2 = -aX1 * aRotateM[2] + aY1 * aRotateM[3];

  // check if the center of Circle is inside the "cross"
  double aHalfSizeAndRadius = aHalfSize + theObj1.GetRadius();
  if ((aX2 > -aHalfSize && aX2 < aHalfSize && aY2 > -aHalfSizeAndRadius && aY2 < aHalfSizeAndRadius) ||
      (aX2 > -aHalfSizeAndRadius && aX2 < aHalfSizeAndRadius && aY2 > -aHalfSize && aY2 < aHalfSize))
  {
    return true;
  }

  return false;
}

bool SquareSquareIntersect(const GravitySquare &theObj1, const GravitySquare &theObj2)
{
  for (int i = 0; i < 2; i++)
  {
    const GravitySquare& anObj1 = i == 0 ? theObj1 : theObj2;
    const GravitySquare& anObj2 = i == 0 ? theObj2 : theObj1;

    // rotate matrix for Square1
    double aRotateM1[4];
    aRotateM1[0] =  cos(anObj1.GetAngle());
    aRotateM1[1] = -sin(anObj1.GetAngle());
    aRotateM1[2] = -aRotateM1[1];
    aRotateM1[3] =  aRotateM1[0];

    // rotate matrix for Square2
    double aRotateM2[4];
    aRotateM2[0] =  cos(anObj2.GetAngle());
    aRotateM2[1] =  sin(anObj2.GetAngle());
    aRotateM2[2] = -aRotateM2[1];
    aRotateM2[3] =  aRotateM2[0];

    // the coords of each corner of Square1
    CoordXY aCorner[4];
    double aHalfSize1 = 0.5 * anObj1.GetLength();
    aCorner[0].x = -aHalfSize1;
    aCorner[0].y = -aHalfSize1;
    aCorner[1].x = -aHalfSize1;
    aCorner[1].y =  aHalfSize1;
    aCorner[2].x =  aHalfSize1;
    aCorner[2].y =  aHalfSize1;
    aCorner[3].x =  aHalfSize1;
    aCorner[3].y = -aHalfSize1;

    double aHalfSize2 = 0.5 * anObj2.GetLength();
    for (int j = 0; j < 4; j++)
    {
      double aX1, aY1, aX2, aY2, aX3, aY3;
      // rotate the corner to Square1 angle
      aX1 = aCorner[j].x * aRotateM1[0] + aCorner[j].y * aRotateM1[1];
      aY1 = aCorner[j].x * aRotateM1[2] + aCorner[j].y * aRotateM1[3];
      // move the corner to Square1 position and back to Square2 position
      aX2 = anObj1.GetPos().x + aX1 - anObj2.GetPos().x;
      aY2 = anObj1.GetPos().y + aY1 - anObj2.GetPos().y;
      // rotate the corner to Square2 -angle
      aX3 = aX2 * aRotateM2[0] + aY2 * aRotateM2[1];
      aY3 = aX2 * aRotateM2[2] + aY2 * aRotateM2[3];
      // check if the corner of Square1 is inside Square2
      if (aX3 > -aHalfSize2 &&
          aX3 <  aHalfSize2 &&
          aY3 > -aHalfSize2 &&
          aY3 <  aHalfSize2)
      {
        return true;
      }
    }
  }

  return false;
}
