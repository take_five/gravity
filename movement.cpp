//
// Created by mints on 26.12.22.
//

#include "movement.h"

Movement::Movement(std::vector<BaseGravityObject*> *theObjs,
                   double theMinX,
                   double theMaxX,
                   double theMinY,
                   double theMaxY,
                   double theTolerance) :
        mMinX(theMinX), mMaxX(theMaxX), mMinY(theMinY), mMaxY(theMaxY), mObjs(theObjs), mTolerance(theTolerance)
{

}

void Movement::Move(double theInterval)
{
  double aLeftInterval = theInterval;

  while (aLeftInterval >= 0.0)
  {
    // find all pairs this collisions
    std::vector<CollisionPair> aPairs;
    for (std::size_t i = 0; i < mObjs->size() - 1; i++)
    {
      BaseGravityObject* anObj1 = (*mObjs)[i];
      for (std::size_t j = i + 1; j < mObjs->size(); j++)
      {
        BaseGravityObject* anObj2 = (*mObjs)[j];
        double aColT;

        auto anIdx = mFlyingApart.find({anObj1, anObj2, 0.0});
        bool isFlyingApart = anIdx != mFlyingApart.end();

        if (FindCollisionOnInterval(*anObj1, *anObj2, aLeftInterval, mTolerance, aColT))
        {
          if (!isFlyingApart)
          {
            CollisionPair aPair(anObj1, anObj2, aColT);
            aPairs.push_back(aPair);
          }
        }
        else
        {
          if (isFlyingApart)
          {
            mFlyingApart.erase(anIdx);
          }
        }
      }
    }

    // if there is no any collision, just move all objects
    if (aPairs.empty())
    {
      for (auto anObj: *mObjs)
      {
        anObj->Move(aLeftInterval);
      }
      return;
    }

    // find the first collision
    double aMinT = aPairs[0].mT;
    std::size_t aColIdx = 0;
    for (std::size_t i = 1; i < aPairs.size(); i++)
    {
      if (aMinT > aPairs[i].mT)
      {
        aMinT = aPairs[i].mT;
        aColIdx = i;
      }
    }

    // move all objects till collision time
    for (auto anObj: *mObjs)
    {
      anObj->Move(aMinT);
    }
    aLeftInterval -= aMinT;

    // change velocities of the collision objects
    MakeRebound(aPairs[aColIdx]);
    mFlyingApart.insert(aPairs[aColIdx]);
  }

  // erase objects that are out of borders
  for (auto i = mObjs->begin(); i != mObjs->end();)
  {
    BaseGravityObject* anObj = *i;
    if (anObj->GetPos().x < mMinX || anObj->GetPos().x > mMaxX ||
        anObj->GetPos().y < mMinY || anObj->GetPos().y > mMaxY)
    {
      for (auto j = mFlyingApart.begin(); j != mFlyingApart.end();)
      {
        const CollisionPair& aPair = *j;
        if (aPair.mObj1 == anObj || aPair.mObj2 == anObj)
        {
          j = mFlyingApart.erase(j);
        }
        else
        {
          j++;
        }
      }
      delete anObj;
      i = mObjs->erase(i);
    }
    else
    {
      i++;
    }
  }
}

void Movement::MakeRebound(const CollisionPair& thePair)
{
  BaseGravityObject* anObj1 = thePair.mObj1;
  BaseGravityObject* anObj2 = thePair.mObj2;

  CoordXY aDir1 {anObj2->GetPos().x - anObj1->GetPos().x, anObj2->GetPos().y - anObj1->GetPos().y};
  aDir1.DoNorm();
  CoordXY aDir2 {-aDir1.y, aDir1.x};

  CoordXY aVel1 {anObj1->GetVel().x * aDir1.x + anObj1->GetVel().y * aDir1.y,
                 anObj1->GetVel().x * aDir2.x + anObj1->GetVel().y * aDir2.y};
  CoordXY aVel2 {anObj2->GetVel().x * aDir1.x + anObj2->GetVel().y * aDir1.y,
                 anObj2->GetVel().x * aDir2.x + anObj2->GetVel().y * aDir2.y};

  // elastic collision
  const double &aM1 = anObj1->GetMass();
  const double &aM2 = anObj2->GetMass();
  double aNewVelX1 = (2.0 * aM2 * aVel2.x + aVel1.x * (aM1 - aM2)) / (aM1 + aM2);
  double aNewVelX2 = (2.0 * aM1 * aVel1.x + aVel2.x * (aM2 - aM1)) / (aM1 + aM2);

  CoordXY aNewVel1 {aDir1.x * aNewVelX1 + aDir2.x * aVel1.y, aDir1.y * aNewVelX1 + aDir2.y * aVel1.y};
  CoordXY aNewVel2 {aDir1.x * aNewVelX2 + aDir2.x * aVel2.y, aDir1.y * aNewVelX2 + aDir2.y * aVel2.y};

  anObj1->SetVel(aNewVel1);
  anObj2->SetVel(aNewVel2);
}

