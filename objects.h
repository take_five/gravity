//
// Created by mints on 25.12.22.
//

#ifndef GRAVITY_APP_OBJECTS_H
#define GRAVITY_APP_OBJECTS_H

#include <cmath>

struct CoordXY
{
  CoordXY() = default;
  CoordXY(double theX, double theY) : x(theX), y(theY) {};
  void DoNorm()
  {
    double aRoot = sqrt(x * x + y * y);
    x /= aRoot;
    y /= aRoot;
  };
  double x;
  double y;
};

class BaseGravityObject
{
public:
  BaseGravityObject() = default;
  BaseGravityObject(double theMass, CoordXY thePos, CoordXY theVel);
  virtual ~BaseGravityObject() = default;
  virtual BaseGravityObject* GetCopy() const = 0;
  void Move(double theInterval);
  static void Move(const CoordXY& theOldPos, const CoordXY& theOldVel, CoordXY& theNewPos, CoordXY& theNewVel, double theInterval);
  static CoordXY GetMaxVel(const CoordXY& theVel, double theInterval);
  inline const CoordXY& GetPos() const
  {
    return mPos;
  };
  inline const CoordXY& GetVel() const
  {
    return mVel;
  };
  inline void SetVel(const CoordXY& theVel)
  {
    mVel = theVel;
  };
  inline const double& GetMass() const
  {
    return mMass;
  };
protected:
  double mMass;
  CoordXY mPos;
  CoordXY mVel;
  static constexpr double mGravityAccel = -9.8;
};

class GravityCircle : public BaseGravityObject
{
public:
  GravityCircle();
  GravityCircle(double theMass, CoordXY thePos, CoordXY theVel, double theRadius);
  BaseGravityObject* GetCopy() const override;
  inline const double& GetRadius() const
  {
    return mRadius;
  };
private:
  double mRadius;
  ~GravityCircle() override = default;
};

class GravitySquare : public BaseGravityObject
{
public:
  GravitySquare();
  GravitySquare(double theMass, CoordXY thePos, CoordXY theVel, double theLength, double theAngle);
  BaseGravityObject* GetCopy() const override;
  inline const double& GetLength() const
  {
    return mLength;
  };
  inline const double& GetAngle() const
  {
    return mAngle;
  };
private:
  double mLength;
  double mAngle;
  ~GravitySquare() override = default;
};

#endif //GRAVITY_APP_OBJECTS_H
