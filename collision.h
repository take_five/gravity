//
// Created by mints on 25.12.22.
//

#ifndef GRAVITY_APP_COLLISION_H
#define GRAVITY_APP_COLLISION_H

#include "objects.h"

bool FindCollisionOnInterval(const BaseGravityObject& theObj1,
                             const BaseGravityObject& theObj2,
                             double theInterval,
                             double theTolerance,
                             double& theFoundT);

bool DetectCollision(const BaseGravityObject &theObj1, const BaseGravityObject &theObj2);

bool CircleCircleIntersect(const GravityCircle &theObj1, const GravityCircle &theObj2);

bool CircleSquareIntersect(const GravityCircle &theObj1, const GravitySquare &theObj2);

bool SquareSquareIntersect(const GravitySquare &theObj1, const GravitySquare &theObj2);

#endif //GRAVITY_APP_COLLISION_H
