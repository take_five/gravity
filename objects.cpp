//
// Created by mints on 25.12.22.
//

#include <algorithm>
#include <cmath>
#include "objects.h"

BaseGravityObject::BaseGravityObject(double theMass, CoordXY thePos, CoordXY theVel) :
        mMass(theMass), mPos(thePos), mVel(theVel)
{
}

void BaseGravityObject::Move(double theInterval)
{
  CoordXY aNewPos;
  CoordXY aNewVel;

  Move(mPos, mVel, aNewPos, aNewVel, theInterval);

  mPos = aNewPos;
  mVel = aNewVel;
}

void BaseGravityObject::Move(const CoordXY& theOldPos, const CoordXY& theOldVel, CoordXY& theNewPos, CoordXY& theNewVel, double theInterval)
{
  theNewPos.x = theOldPos.x + theOldVel.x * theInterval;
  theNewPos.y = theOldPos.y + theOldVel.y * theInterval + 0.5 * mGravityAccel * theInterval * theInterval;
  theNewVel.x = theOldVel.x;
  theNewVel.y = theOldVel.y + mGravityAccel * theInterval;
}

CoordXY BaseGravityObject::GetMaxVel(const CoordXY& theVel, double theInterval)
{
  double aNewVelY = theVel.y + mGravityAccel * theInterval;

  if (fabs(theVel.y) > fabs(aNewVelY))
  {
    return {theVel.x, theVel.y};
  }
  else
  {
    return {theVel.x, aNewVelY};
  }
}

GravityCircle::GravityCircle() : BaseGravityObject()
{
}

GravityCircle::GravityCircle(double theMass, CoordXY thePos, CoordXY theVel, double theRadius) :
        BaseGravityObject(theMass, thePos, theVel), mRadius(theRadius)
{
}

BaseGravityObject* GravityCircle::GetCopy() const
{
  auto aPtr = new GravityCircle();
  *aPtr = *this;
  return aPtr;
}

GravitySquare::GravitySquare() : BaseGravityObject()
{
}

GravitySquare::GravitySquare(double theMass, CoordXY thePos, CoordXY theVel, double theLength, double theAngle):
        BaseGravityObject(theMass, thePos, theVel), mLength(theLength), mAngle(theAngle)
{
}

BaseGravityObject* GravitySquare::GetCopy() const
{
  auto aPtr = new GravitySquare();
  *aPtr = *this;
  return aPtr;
}



