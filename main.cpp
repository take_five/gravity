#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <cmath>
#include "movement.h"

void RenderingThread(sf::RenderWindow* theWindow)
{
  // activate the window's context
  theWindow->setActive(true);

  double aScale = 10.0;
  double aSizeX = (double)theWindow->getSize().x / aScale;
  double aSizeY = (double)theWindow->getSize().y / aScale;

  std::vector<BaseGravityObject*> anObjs;
  anObjs.push_back(new GravityCircle(100.0, {0.25 * aSizeX, aSizeY}, {1.0, 0.0}, 1.0));
  anObjs.push_back(new GravityCircle(100.0, {0.75 * aSizeX, aSizeY}, {-1.0, 0.0}, 1.0));
  anObjs.push_back(new GravityCircle(1.0, {0.5 * aSizeX, aSizeY}, {20.0, 0.0}, 2.0));
  anObjs.push_back(new GravitySquare(1.0, {0.2 * aSizeX, aSizeY}, {10.0, 0.0}, 1.0, 0.3));
  anObjs.push_back(new GravitySquare(10.0, {0.8 * aSizeX, aSizeY}, {-5.0, 0.0}, 2.0, 0.6));
  anObjs.push_back(new GravitySquare(5.0, {0.0, 0.0}, {10.0, 37.0}, 1.0, 0.0));

  Movement aMovement(&anObjs, 0.0, aSizeX, 0.0, aSizeY, 1.0 / aScale);

  // the rendering loop
  while (theWindow->isOpen())
  {
    // clear the window with black color
    theWindow->clear(sf::Color::Black);
    // draw all objects
    int aCnt = 0;
    for (auto anObj : anObjs)
    {
      aCnt += 1;
      auto aGrCircle = dynamic_cast<GravityCircle*>(anObj);
      if (aGrCircle != nullptr)
      {
        sf::CircleShape aCircle(aGrCircle->GetRadius() * aScale);
        aCircle.setFillColor(sf::Color(100, 250, aCnt * 50));
        aCircle.move((aGrCircle->GetPos().x - aGrCircle->GetRadius()) * aScale,
                     (aSizeY - (aGrCircle->GetPos().y - aGrCircle->GetRadius())) * aScale);
        theWindow->draw(aCircle);
        continue;
      }
      auto aGrSquare = dynamic_cast<GravitySquare*>(anObj);
      if (aGrSquare != nullptr)
      {
        sf::RectangleShape aSquare(sf::Vector2f(aGrSquare->GetLength() * aScale, aGrSquare->GetLength() * aScale));
        aSquare.setFillColor(sf::Color(200, 50, 50));
        aSquare.rotate(aGrSquare->GetAngle() * 180.0 / M_PI);
        aSquare.move((aGrSquare->GetPos().x - 0.5 * aGrSquare->GetLength()) * aScale,
                     (aSizeY - (aGrSquare->GetPos().y - 0.5 * aGrSquare->GetLength())) * aScale);
        theWindow->draw(aSquare);
      }
    }
    // end the current frame
    theWindow->display();
    //
    sf::sleep(sf::milliseconds(50));
    // move shapes
    aMovement.Move(0.05);
  }
}

int main()
{
  // create the aWindow (remember: it's safer to create it in the main aThread due to OS limitations)
  sf::RenderWindow aWindow(sf::VideoMode(1000, 1000), "OpenGL");

  // deactivate its OpenGL context
  aWindow.setActive(false);

  // launch the rendering aThread
  sf::Thread aThread(&RenderingThread, &aWindow);
  aThread.launch();

  // the event/logic/whatever loop
  while (aWindow.isOpen())
  {
    // check all the window's events that were triggered since the last iteration of the loop
    sf::Event anEvent;
    while (aWindow.pollEvent(anEvent))
    {
      // "close requested" anEvent: we close the window
      if (anEvent.type == sf::Event::Closed)
        aWindow.close();
    }
  }

  return 0;
}
